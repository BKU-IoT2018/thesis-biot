const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const dweetClient = require('node-dweetio');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const moment = require('moment');
var logger = require('morgan');
const dweetio = new dweetClient();
const dweetThing = 'node-temperature-monitor';
const SERVER_PORT = 3000;
var engine = require('consolidate');
var firebase = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");
//encrypt ethereum
const EthCrypto = require('eth-crypto');
const privateKey = "0x9847C2B7865F1FCF3177753B30EB20C91EB102F019CB3A66352E248E378A24FF";
const publicKey = EthCrypto.publicKeyByPrivateKey(privateKey);
console.log("publickey",publicKey);
const axios = require('axios');
//console.log(identity.privateKey);
var idhumid = 0;
var idtemp = 0;
var datadecrypttemp = [];
var datadecrypthumid = [];
//firebase init
var firebaseAdmin = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://esp-server.firebaseio.com"
});

//hash data
var hash = require('object-hash');

//time
//web3 and sign transaction
require('dotenv').config();
const Web3 = require('web3')
const EthereumTx = require('ethereumjs-tx');
const log = require('ololog').configure({ time: true })
const ansi = require('ansicolor').nice
const testnet = `https://ropsten.infura.io/v3/aa3fc6be44b04979acb8380cf5324d5b`;
const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/a81c04b5c0d3438fa1721374eb5e66c6'));
const contractABI = [ { "constant": false, "inputs": [ { "name": "owner", "type": "address" }, { "name": "txId", "type": "bytes32" } ], "name": "confirmReceivedData", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "requester", "type": "address" }, { "name": "txId", "type": "bytes32" }, { "name": "dataHash", "type": "bytes32" } ], "name": "confirmSentData", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "deviceId", "type": "bytes32" }, { "name": "dailyPrice", "type": "uint256" } ], "name": "registerDevice", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "owner", "type": "address" }, { "name": "_deviceId", "type": "bytes32" }, { "name": "publicKey", "type": "bytes" }, { "name": "fromTime", "type": "uint256" }, { "name": "toTime", "type": "uint256" }, { "name": "api", "type": "string" } ], "name": "requestData", "outputs": [ { "name": "", "type": "bool" } ], "payable": true, "stateMutability": "payable", "type": "function" }, { "inputs": [ { "name": "_minInterval", "type": "uint256" } ], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "owner", "type": "address" }, { "indexed": false, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "dailyPrice", "type": "uint256" } ], "name": "NewDeviceAdded", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "publicKey", "type": "bytes" }, { "indexed": false, "name": "time", "type": "uint256" }, { "indexed": false, "name": "fromTime", "type": "uint256" }, { "indexed": false, "name": "toTime", "type": "uint256" }, { "indexed": false, "name": "api", "type": "string" }, { "indexed": false, "name": "txId", "type": "bytes32" } ], "name": "RequestDataSuccess", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "description", "type": "string" }, { "indexed": false, "name": "money", "type": "uint256" }, { "indexed": false, "name": "time", "type": "uint256" } ], "name": "RequestDataFail", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "txId", "type": "bytes32" } ], "name": "DataSent", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "txId", "type": "bytes32" } ], "name": "DataReceived", "type": "event" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "address" }, { "name": "", "type": "uint256" } ], "name": "badRequestList", "outputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" }, { "name": "deviceId", "type": "bytes32" }, { "name": "time", "type": "uint256" }, { "name": "banUntil", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" } ], "name": "getBadRequestListLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "txId", "type": "bytes32" } ], "name": "getDeposit", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" } ], "name": "getTransactionListLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "user", "type": "address" } ], "name": "getUserDevicesLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getUsers", "outputs": [ { "name": "", "type": "address[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "minInterval", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "address" } ], "name": "timeOfLastRequest", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "uint256" } ], "name": "userDevices", "outputs": [ { "name": "deviceId", "type": "bytes32" }, { "name": "dailyPrice", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "users", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" } ];

const address='0xa800baa3383bd2dfd1516093ce9c1c28258cdf5a';
const contractDeployed = new web3.eth.Contract(contractABI, address);

async function confirmDataSend(addressrequest, txID, hashData){
  let nonce = await web3.eth.getTransactionCount(process.env.WALLET_ADDRESS)
  const deviceId = "0x9543231313835140000000000000000000000000000000000000000000000000";
  // var data = await web3.eth.Contract(contractABI, address, {gasPrice: '12345678', defaultAccount: process.env.WALLET_ADDRESS}).methods.confirmSentData(addressrequest, txID, hashData).encodeABI()
  const data = contractDeployed.methods.confirmSentData(addressrequest, txID, hashData).encodeABI();
  console.log(process.env.WALLET_ADDRESS, "OK");
  const txParams = {
    nonce: nonce,
    gasPrice: web3.utils.toHex(web3.utils.toWei('50', 'gwei')),
    gasLimit: 4000000,
    to: address,
    data: data,
  }
  let tx =  new EthereumTx(txParams);
  let privateK =  Buffer.from('9847C2B7865F1FCF3177753B30EB20C91EB102F019CB3A66352E248E378A24FF', 'hex');

   tx.sign(privateK);
 
  const rawTx = '0x' + tx.serialize().toString('hex');

  console.log("tx", rawTx)
  web3.eth.sendSignedTransaction(rawTx,
    (err, result) => {
      if(err){
        console.log("err",err)
      }
      else console.log("result",result);
    }
)
  }
//confirmDataSend("0xd2bbf548c91550a3f1e3bc2df18e3be6e973b2b9","0x1593f8d02d85e438fee280763a0b416882b1da86ba32f92e2f1c49546980a192","0x67b69634f9880a282c14a0f0cb7ba20cf5d677e1000000000000000000000000")

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(logger('dev'))

app.use(bodyParser.json());
app.use(express.static('views'));
app.set('views', __dirname + '/views')
app.engine('html', engine.mustache);
app.set('view engine', 'html');
app.get('/', (req, res) => {
  // res.sendFile(__dirname + 'index.html');
  res.render('home.html')
});
app.get('/', (req, res) => {
  // res.sendFile(__dirname + 'index.html');
  res.render('home.html')
});

app.use((req, res) => {
  res.status(404).json({
    message: 'Resource not found'
  });
});

io.on('connection', (socket) => {
  console.log('Connection has been established with browser.');
socket.on('info-request', function (inforequest) {
    console.log("Received data", inforequest);
    //decryptData(inforequest)
    getDatafromFireBase(inforequest)
      .then(function () {
        console.log("Success get data from firebase")
      })
      .catch(function (error) {
        console.log(error)
      });
  });


  socket.on('disconnect', () => {
    console.log('Browser client disconnected from the connection.');
  });
});

Object.size = function (obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

async function encryptTempdata(value, time, deviceid) {
  var database = firebase.database()
  var TempRef = database.ref(`/${deviceid}/Temp`)
  var TempRefChild = TempRef.child(`${idtemp}`);
  const encrypttemp = await EthCrypto.encryptWithPublicKey(publicKey, `${value}`);
  const encrypttime = await EthCrypto.encryptWithPublicKey(publicKey, `${time}`);

  await TempRefChild.set({
    value: encrypttemp,
    times: encrypttime,
  })
    .then(function () {
      console.log("Push Temp data")
      idtemp = idtemp + 1;
    })
    .catch(function (error) {
      console.log(error)
    })
}
async function getDatafromFireBase(inforequest){
  //const deviceid = inforequest.deviveid;
  var db = firebase.database();
  var ref = db.ref("/");
  await ref.on("value", function (snapshot) {
  var data = snapshot.val();
   // deviceidd ='0x1234000000000000000000000000000000000000000000000000000000000000';
   //console.log('Check Data',data[`${deviceidd}`].Temp);
   decryptData(inforequest,data);
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });
  
}

async function decryptData(inforequest,data) {
  datadecrypttemp = [];
  datadecrypthumid = [];
  //const data = await getDatafromFireBase();
  //var data = [];
  //var db = firebase.database();
  //var ref = db.ref("/");
  const deviceid = inforequest.deviveid;
 // console.log('Firebase',deviceid);
  //const requestKey = inforequest.publickey;
  const requestKey = "da7162e41f566522f270d056c0e1296aa18c7428625688e4d341e5baea273feea9f420d7cc54dea586fb12512724151f234063f7ff025cca8361ee14613346a7";
  const starttime = inforequest.starttime;
  const endtime = inforequest.endtime;
  const api = inforequest.api;
  const requester = inforequest.requester;
  const txID = inforequest.txId;
  // Attach an asynchronous callback to read the data at our posts reference
  // await ref.on("value", function (snapshot) {
  //   return snapshot.val();
  //  // deviceidd ='0x1234000000000000000000000000000000000000000000000000000000000000';
  //  // console.log('Check Data',data[`${deviceidd}`].Temp);
  //   console.log('data', data);
  // }, function (errorObject) {
  //   console.log("The read failed: " + errorObject.code);
  // });
  //console.log('Du lieu lay ve',data);
  var i = 0;
  size = await Object.size(data[`${deviceid}`].Temp);
  for (i; i < size; i++) {
    if (data[`${deviceid}`].Temp[i]) {
      var decryptdata = await EthCrypto.decryptWithPrivateKey(privateKey, data[`${deviceid}`].Temp[i]['times']);
      if (decryptdata <= endtime && decryptdata >= starttime) {
        datadecrypttemp.push(decryptdata);
        decryptdata = await EthCrypto.decryptWithPrivateKey(privateKey, data[`${deviceid}`].Temp[i]['value']);
        datadecrypttemp.push(decryptdata);
      }
    }
  }
  var j = 0;
  sizehumid = Object.size(data[`${deviceid}`].Humi);
  for (j; j < sizehumid; j++) {
    if (data[`${deviceid}`].Humi[j]) {
      var decryptdatahumid = await EthCrypto.decryptWithPrivateKey(privateKey, data[`${deviceid}`].Humi[j]['times']);
      if (decryptdatahumid <= endtime && decryptdatahumid >= starttime) {
        datadecrypthumid.push(decryptdatahumid);
        decryptdatahumid = await EthCrypto.decryptWithPrivateKey(privateKey, data[`${deviceid}`].Humi[j]['value']);
        datadecrypthumid.push(decryptdatahumid);
      }
    }
  }
  const combinedata = {
    "Temp": datadecrypttemp,
    "Humi": datadecrypthumid,
  }
  const convertdata = JSON.stringify(combinedata);
  const hashData = await hash(convertdata);
 
  
  const encryptfinalData = await EthCrypto.encryptWithPublicKey(requestKey, convertdata);
  const changeString = EthCrypto.cipher.stringify(encryptfinalData);
  const finalData = {
    "hash": hashData,
    "data": changeString
  }
  console.log("finaldata",finalData);
  sendData(finalData,api,requester,txID);
}
function sendData(data,api, requester, txID) {
  console.log("Sendata",data);
  axios.post(api, data)
  .then(function (response) {
    const hashdata = '0x' + data.hash + '000000000000000000000000';
    confirmDataSend(requester, txID, hashdata);
    console.log("Success Send data");
  })
  .catch(function (error) {
    console.log(error);
  });
}
async function encryptHumidata(value, time, deviceid) {
  var database = firebase.database()
  var HumidRef = database.ref(`/${deviceid}/Humi`)
  const encrypthumid = await EthCrypto.encryptWithPublicKey(publicKey, `${value}`);
 
  const encrypttime = await EthCrypto.encryptWithPublicKey(publicKey, `${time}`);
  var HumidRefChild = HumidRef.child(`${idhumid}`);
  
  await HumidRefChild.set({
    value: encrypthumid,
    times: encrypttime,
  })
    .then(function () {
      console.log("Push Humid data")
      idhumid = idhumid + 1;
    })
    .catch(function (error) {
      console.log(error)
    })
    //console.log("Push Humid data")
}
dweetio.listen_for(dweetThing, async (dweet) => {

  const data = {
    sensorData: dweet.content,
    time: Math.floor(new Date().getTime() / 1000.0)
  };

  // time: moment().format("YYYY-MM-DD HH:mm:ss")
  console.log("data check", data)
//  console.log(data.sensorData.humidity);
 // console.log(data.sensorData.temperature);
//  console.log(data.time);
  var temp = data.sensorData.temperature;
  var humid = data.sensorData.humidity;
  var time = data.time;
  var deviceidx = "0x" + data.sensorData.deviceId;
  console.log(deviceidx)
  //if(temp && humid && time){
    
    await encryptTempdata(temp, time, deviceidx);
    await encryptHumidata(humid, time, deviceidx);
    io.emit('sensor-data', data);
  //}
  
});

http.listen(process.env.PORT || SERVER_PORT, () => {
  console.log(`Server started on the http://localhost:${SERVER_PORT}`);
});
