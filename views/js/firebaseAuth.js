function checkIfLoggedIn() {
    firebase.auth().onAuthStateChanged( function (user) {
        if (user) {
            console.log("User signned in")
            console.log(user)
            var photoURL = user.photoURL;
            document.getElementById('signout')
                .setAttribute('style', 'display: inline-block; visibility: visible')
            document.getElementById('google-signin')
                .setAttribute('style', 'display: none; visibility: hidden')
            document.getElementById("displayname").innerHTML = user.displayName;
            document.getElementById("google-email").innerHTML = user.email;
            document.getElementById('google-pic')
                .setAttribute('src', photoURL)
        } else {
            document.getElementById('google-signin')
                .setAttribute('style', 'display: inline-block; visibility: visible')
            document.getElementById('signout')
                .setAttribute('style', 'display: none; visibility: hidden')
        }
    }
)
    //if (localStorage.getItem('firebase_idToken')) {
}
window.onload = function () {
    checkIfLoggedIn();
    document.getElementById('signout') .setAttribute('style', 'display: none; visibility: hidden')
}
function signInWithGoogle() {
    var googleAuthProvider = new firebase.auth.GoogleAuthProvider

    firebase.auth().signInWithPopup(googleAuthProvider)
        .then(function (data) {
            console.log(data);
            //Set the imahe, p, and h3 to be the profile of user
            //var idToken = data.credential.idToken;
            //localStorage.setItem('firebase_idToken', idToken)
            //localStorage.setItem('google_photo', data.user.photoURL);
            var string1 = "kalosnguyen@gmail.com";
            var string2 = data.user.email;
            var result = string1.localeCompare(string2);
            if(result===0){
                var signincard = $("#InputforOwner");
                signincard.show();
                var profilecard = $("#profilecard");
                profilecard.show();
                document.getElementById("displayname").innerHTML = data.user.displayName;
                document.getElementById("google-pic").src = data.user.photoURL;
                document.getElementById("google-email").innerHTML = data.user.email;
                checkIfLoggedIn();
            }
            else{
                signOut();
                window.location.reload()
            }
        })
        .catch(function (error) {
            console.log(error)
        })
}
function signOut() {
    console.log("User signned out")
    // localStorage.removeItem('firebase_idToken')
    // localStorage.removeItem('google_photo')
    firebase.auth().signOut();
    document.getElementById('google-pic')
        .setAttribute('src', '')
        window.location.reload()
    checkIfLoggedIn();
}
