const socket = io();
let datatemp = [];
let datahumid=[];
let chart = Morris.Line({
    element: 'line-example',
    datatemp,
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Time'],
    parseTime: false,
    pointFillColors: ['white'],
    pointStrokeColors: ['white'],
    lineColors: ['red'],
    eventLineColors:['white'],
    goalLineColors: ['white']
});
let charthumid = Morris.Line({
    element: 'line-example-humid',
    datahumid,
    xkey: 'y',
    ykeys: ['a'],
    labels: ['Time'],
    parseTime: false,
    pointFillColors: ['white'],
    pointStrokeColors: ['white'],
    lineColors: ['blue'],
    eventLineColors:['white'],
    goalLineColors: ['white']
});
socket.on('sensor-data', (content) => {
    //let template = "<tr><td>" + content.sensorData.temperature + "ºC</td>" +"<td>" + content.sensorData.humidity + "</td>" + "<td>" + content.time + "</td></tr>"
    let template = "<tr><th>" + content.sensorData.temperature + "ºC</th>" +"<th>" + content.sensorData.humidity + "</th>"+ "<th>" + content.time + "</th></tr> ";
    
    datatemp.push({
        y: content.time,
        a: content.sensorData.temperature
    });
    datahumid.push({
        y: content.time,
        a: content.sensorData.humidity
    });
    $('.table-body').append(template);
    chart.setData(datatemp);
    charthumid.setData(datahumid);
});