const contractABI = [ { "constant": false, "inputs": [ { "name": "owner", "type": "address" }, { "name": "txId", "type": "bytes32" } ], "name": "confirmReceivedData", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "requester", "type": "address" }, { "name": "txId", "type": "bytes32" }, { "name": "dataHash", "type": "bytes32" } ], "name": "confirmSentData", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "deviceId", "type": "bytes32" }, { "name": "dailyPrice", "type": "uint256" } ], "name": "registerDevice", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "owner", "type": "address" }, { "name": "_deviceId", "type": "bytes32" }, { "name": "publicKey", "type": "bytes" }, { "name": "fromTime", "type": "uint256" }, { "name": "toTime", "type": "uint256" }, { "name": "api", "type": "string" } ], "name": "requestData", "outputs": [ { "name": "", "type": "bool" } ], "payable": true, "stateMutability": "payable", "type": "function" }, { "inputs": [ { "name": "_minInterval", "type": "uint256" } ], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "owner", "type": "address" }, { "indexed": false, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "dailyPrice", "type": "uint256" } ], "name": "NewDeviceAdded", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "publicKey", "type": "bytes" }, { "indexed": false, "name": "time", "type": "uint256" }, { "indexed": false, "name": "fromTime", "type": "uint256" }, { "indexed": false, "name": "toTime", "type": "uint256" }, { "indexed": false, "name": "api", "type": "string" }, { "indexed": false, "name": "txId", "type": "bytes32" } ], "name": "RequestDataSuccess", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "deviceId", "type": "bytes32" }, { "indexed": false, "name": "description", "type": "string" }, { "indexed": false, "name": "money", "type": "uint256" }, { "indexed": false, "name": "time", "type": "uint256" } ], "name": "RequestDataFail", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "txId", "type": "bytes32" } ], "name": "DataSent", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "requester", "type": "address" }, { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "txId", "type": "bytes32" } ], "name": "DataReceived", "type": "event" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "address" }, { "name": "", "type": "uint256" } ], "name": "badRequestList", "outputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" }, { "name": "deviceId", "type": "bytes32" }, { "name": "time", "type": "uint256" }, { "name": "banUntil", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" } ], "name": "getBadRequestListLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "txId", "type": "bytes32" } ], "name": "getDeposit", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "requester", "type": "address" }, { "name": "owner", "type": "address" } ], "name": "getTransactionListLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "user", "type": "address" } ], "name": "getUserDevicesLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getUsers", "outputs": [ { "name": "", "type": "address[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "minInterval", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "address" } ], "name": "timeOfLastRequest", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "address" }, { "name": "", "type": "uint256" } ], "name": "userDevices", "outputs": [ { "name": "deviceId", "type": "bytes32" }, { "name": "dailyPrice", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "", "type": "uint256" } ], "name": "users", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
if (typeof (web3) !== undefined) {
  web3 = new Web3(web3.currentProvider);
} else {
  web3 = new Web3(new Web3.providers.WebsocketProvider("wss://rinkeby.infura.io/ws"));
  //web3 = new Web3(new Web3.providers.WebsocketProvider("http://localhost:7545"));
}
console.log(web3.currentProvider);
const contract_address = '0xa800baa3383bd2dfd1516093ce9c1c28258cdf5a';
var version = web3.version.api;
console.log(version); // "0.2.02536
const instance = web3.eth.contract(contractABI).at(contract_address);
console.log(instance);
const defaultAccount = web3.eth.accounts[0];
console.log(defaultAccount);

function Registerdevice() {
  var addressofdevice = $('#adddevice').val();
  var dailyprice = $('#pricedaily').val();
  dailypricetrade = web3.toWei(dailyprice, 'ether')
  instance.registerDevice(addressofdevice, dailypricetrade, { from: defaultAccount, gas: 3000000 }, (err, txHash) => {
    if (err) {
      console.log(err)
    }
    else {
      console.log(txHash)
      $('#adddevice').val("");
      $('#pricedaily').val("");
    }
  })
}
function ConfirmSentData(){
  var addressrequest = $('#addresSuccess').val();
  var txID = $('#txIDSuccess').val();
  var hashData = $('#hashSuccess').val();
  instance.confirmSentData(addressrequest, txID, hashData,{ from: defaultAccount, gas: 3000000}, (err, result) => {
    if (err) {
      console.log(err)
    }
    else {
      console.log(result);
    }
  })
}
function requestDataOwner() {
  var aowner = $('#owneraddress').val();
  var deviceidd = $('#deviceidd').val();
  var publickey = $("#publickeyre").val();
  var timestart = $("#timestart").val();
  var timeend = $("#timeend").val();
  var api = $("#apire").val();
  console.log(aowner);
  console.log(deviceidd);
  console.log(publickey);
  console.log(timestart);
  console.log(timeend);
  console.log(api);
  instance.requestData(aowner, deviceidd, publickey, timestart, timeend, api, { from: defaultAccount, gas: 3000000, value: web3.toWei(0.05, "ether") }, (err, result) => {
    if (err) {
      console.log(err)
    }
    else {
      console.log(result);
    }
  })
}
function getDevices() {
  var addressofuser =defaultAccount;
  instance.getUserDevicesLength(addressofuser, { from: defaultAccount, gas: 3000000 }, (err, result) => {
    if (err) {
      console.log(err)
    }
    else {
      console.log(result);
      var size = result.c[0];
      console.log(size);
      var i;
      for (i = 0; i < size; i++) {
        instance.userDevices.call(defaultAccount, i, function (err, result) {
          if (!err) {
            console.log(result[0])
            var stt = 0;
            let template = "<tr><th>" + result[0] + "</th></tr> ";
          
            $('.device-body').append(template);
          }
        })
      }
      $('#addusers').val("");
    }
  })
}


var requestDataSuccess = instance.RequestDataSuccess({ owner: defaultAccount }, { fromBlock: 'latest' });
var requestDataAll = instance.RequestDataSuccess({ owner: defaultAccount }, { fromBlock: '0', toBlock: "latest" });
requestDataAll.watch((err, res) => {
  if (err) {
    console.log(err)
  }
  else {
    console.log(res);
    let template = "<tr><th>"+ res.args.requester + "</th>" + "<th>" + res.args.deviceId + "</th></tr> ";
          
            $('.request-body').append(template);
   
  }
});
requestDataSuccess.watch((err, res) => {
  if (err) {
    console.log(err)
  }
  else {
    console.log(res);
    console.log(res.args.publicKey);
    console.log(res.args.api);
    console.log(res.args.fromTime.c[0]);
    console.log(res.args.toTime.c[0])
    console.log(res.args.deviceId)
    const inforequest = {
      "requester": res.args.requester,
      "publickey": res.args.publicKey,
      "api": res.args.api,
      "starttime": res.args.fromTime.c[0],
      "endtime": res.args.toTime.c[0],
      "deviveid": res.args.deviceId,
      'txId': res.args.txId,
    }
    socket.emit('info-request', inforequest);
  }
});
var DataSentSuccess = instance.DataSent({ owner: defaultAccount }, { fromBlock: '0', toBlock: "latest" });
DataSentSuccess.watch((err, res) => {
  if (err) {
    console.log(err)
  }
  else {
    console.log(res);
    let template = "<tr><th>"+ res.args.requester + "</th>" + "<th>" + res.args.txId + "</th></tr> ";
          
            $('.success-send-body').append(template);
   
  }
});