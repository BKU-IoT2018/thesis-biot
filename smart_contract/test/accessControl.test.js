const AccessControl = artifacts.require('AccessControl');
const Web3 = require('web3');
const web3 = new Web3(
  Web3.givenProvider ||
    new Web3.providers.WebsocketProvider('ws://localhost:7545'),
  null,
  {}
);

let instance;

beforeEach(async () => {
  instance = await AccessControl.deployed();
});

contract('test AccessControl contract', async accounts => {
  it('deploy ok', async () => {
    //console.log(instance)
    let minInterval = await instance.minInterval();
    assert.equal(minInterval.toNumber(), 5);
  });

  it('should register data ok', async () => {
    let a = await instance.registerDevice('0x11', 2000000000);
    let account0Devices = await instance.userDevices(accounts[0], 0);

    assert.equal(account0Devices.dailyPrice.toNumber(), 2000000000);
  });

  it('should request data success', async () => {
    let a = await instance.registerDevice('0x11', 2000000000);
    let b = await instance.requestData(
      accounts[0],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[1], value: 5000000000 }
    );

    assert.equal(b.receipt.logs[0].args.api, 'api');
  });

  it('require a fromTime < toTime', async () => {
    try {
      await instance.requestData(
        accounts[0],
        '0x10',
        '0x1234',
        1554597924,
        1554507924,
        'api',
        { from: accounts[1], value: 5000000000 }
      );
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('should get device does not exist - wrong device Id', async () => {
    let a = await instance.registerDevice('0x11', 2000000000);
    let b = await instance.requestData(
      accounts[0],
      '0x10',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[1], value: 5000000000 }
    );

    assert.equal(b.receipt.logs[0].args.description, "Device doesn't exist");
  });

  it('should get device does not exist 2 - wrong owner', async () => {
    let a = await instance.registerDevice('0x11', 2000000000);
    let b = await instance.requestData(
      accounts[1],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[2], value: 5000000000 }
    );

    assert.equal(b.receipt.logs[0].args.description, "Device doesn't exist");
  });

  it('should get not enough money', async () => {
    let a = await instance.registerDevice('0x11', 2000);
    let b = await instance.requestData(
      accounts[0],
      '0x11',
      '0x1234',
      1557303503,
      1557562703,
      'api',
      { from: accounts[3], value: 7001 }
    );
    console.log(b.receipt.logs);
    assert.equal(b.receipt.logs[0].args.description, 'Not enough money');
  });

  /// TEST BAD REQUEST
  it('should get bad request with 30 seconds', async () => {
    let a = await instance.registerDevice('0x11', 2000000000, {
      from: accounts[1]
    });
    let request1 = await instance.requestData(
      accounts[1],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[4], value: 5000000000 }
    );
    let request2 = await instance.requestData(
      accounts[1],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[4], value: 5000000000 }
    );
    let eventArgs = request2.receipt.logs[0].args;

    assert.equal(eventArgs.description, 'Bad request');

    let badRequestListLength = await instance.getBadRequestListLength(
      accounts[4],
      accounts[1]
    );
    let lastBadRequest = await instance.badRequestList(
      accounts[4],
      accounts[1],
      badRequestListLength - 1
    );
    assert.equal(
      lastBadRequest.banUntil.toNumber() - lastBadRequest.time.toNumber(),
      30
    );
  });

  it('should get bad request with length * 2 minutes', async () => {
    let a = await instance.registerDevice('0x11', 2000000000, {
      from: accounts[2]
    });
    let request1 = await instance.requestData(
      accounts[2],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[4], value: 5000000000 }
    );
    let request2 = await instance.requestData(
      accounts[2],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[4], value: 5000000000 }
    );
    let request3 = await instance.requestData(
      accounts[2],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[4], value: 5000000000 }
    );
    let eventArgs = request3.receipt.logs[0].args;

    assert.equal(eventArgs.description, 'Bad request');

    let badRequestListLength = await instance.getBadRequestListLength(
      accounts[4],
      accounts[2]
    );
    let lastBadRequest = await instance.badRequestList(
      accounts[4],
      accounts[2],
      badRequestListLength - 1
    );
    assert.equal(
      lastBadRequest.banUntil.toNumber() - lastBadRequest.time.toNumber(),
      120
    );
  });

  it('check money flow', async () => {
    let a = await instance.registerDevice('0x11', 2000000000);
    let b = await instance.requestData(
      accounts[0],
      '0x11',
      '0x1234',
      1554507924,
      1554597924,
      'api',
      { from: accounts[5], value: 5000000000000000000 }
    );

    //check balance account5 deposit account0
    let acc5DepositAcc0 = await instance.getDeposit(accounts[5]);
    assert.equal(acc5DepositAcc0.toString(), '5000000000000000000');

    //check balance account0 receive
    let d = await instance.confirmReceivedData(accounts[0], {
      from: accounts[5]
    });

    //check deposit balance return 0
    acc5DepositAcc0 = await instance.getDeposit(accounts[5]);
    assert.equal(acc5DepositAcc0.toString(), '0');
  });
});

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
